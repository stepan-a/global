function approximation(order)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
    
nodes = chebyshev_roots(order+1);
y = feval('truefunction', nodes);
x = evaluate_chebyshev_monomials(nodes, order);
coefficients = x\y;
x0 = transpose(-1:.001:1);
y0 = evaluate_chebyshev_polynomial(x0,coefficients);
y1 = feval('truefunction',x0);
figure(1)
plot(x0, y1,'-r','linewidth',2)
hold on
plot(x0, y0,'--k','linewidth',2)
hold off
figure(2)
plot(x0, y1-y0,'-r','linewidth',2)
disp(sprintf('Max. abs. error is %s',num2str(max(abs(y0-y1)))))
    
    
    