function approximation(order)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

alpha = .3;
gamma = .5;

kinf = 0.01;
linf = 0.01;
ksup = 5.00;
lsup = 5.00;

nodes = chebyshev_roots(order+1);

knodes = (nodes+1)*(ksup-kinf)/2+kinf;
lnodes = (nodes+1)*(lsup-linf)/2+linf;
vnodes = cartesian_product_of_sets(knodes, lnodes); 

y = feval('truefunction', vnodes(:,1), vnodes(:,2), alpha, gamma);

powers = build_tensor_grid_powers(2, order);
nodes_ = cartesian_product_of_sets(nodes, nodes);

x = evaluate_chebyshev_multivariate_monomials({nodes_(:,1) nodes_(:,2)}, powers+1);

coefficients = x\y;

nodes = transpose(-1:.1:1);
knodes0 = (nodes+1)*(ksup-kinf)/2+kinf;
lnodes0 = (nodes+1)*(lsup-linf)/2+linf;
vnodes0 = cartesian_product_of_sets(knodes0, lnodes0); 
nodes_ = cartesian_product_of_sets(nodes, nodes);
y0 = evaluate_chebyshev_multivariate_polynomial(nodes_, coefficients, powers);
y1 = feval('truefunction',vnodes0(:,1),vnodes0(:,2),alpha,gamma);


figure(1)
hold on
surf(knodes0, lnodes0, reshape(y0,length(nodes),length(nodes)))
surf(knodes0, lnodes0, reshape(y1,length(nodes),length(nodes)))
xlabel('Capital')
ylabel('Labour')
zlabel('Output')
hold off
figure(2)
surf(knodes0, lnodes0, reshape(y1-y0,length(nodes),length(nodes)))
xlabel('Capital')
ylabel('Labour')
zlabel('Error')

disp(sprintf('Max. abs. error is %s',num2str(max(abs(y0-y1)))))
    
    
    