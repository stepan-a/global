function y = truefunction(k, l, gamma, alpha)
    y = (alpha*k.^gamma+(1-alpha)*l.^gamma).^(1/gamma);