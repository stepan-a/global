function orders = build_complete_grid_powers(n,d)

% INPUTS     
% n   [integer]  scalar, number of variables.
% d   [integer]  scalar, degree of approximation.

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

TotalNumberOfNodes = NaN(d+1,1);
TotalNumberOfNodes(1) = 1;% constant term.
for i=1:d
    TotalNumberOfNodes(i+1) = combinations_with_repetitions(n,i); 
end

orders = NaN(sum(TotalNumberOfNodes),n);

ground = 0;
for i=0:d
    orders(ground+1:ground+TotalNumberOfNodes(i+1),:) = get_positive_integer_powers_summing_up_to_p(n,i,0);
    ground = ground+TotalNumberOfNodes(i+1);
end