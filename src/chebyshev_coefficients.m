function c = chebyshev_coefficients(order, allorders)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

if nargin<2
    allorders = true;
end

if allorders
    c = zeros(order+1);
    c(1,1) = 1;
    if order>0
        c(2,2) = 1;
    end
    for i=2:order
        c(i+1,1) = -1.0*c(i-1,1);
        c(i+1,i+1) = 2.0*c(i,i);
        if isequal(c(i+1,1), 0)
            for j=2:2:i
                c(i+1,j) = 2.0*c(i,j-1) - c(i-1,j);
            end
        else
            for j=3:2:i
                c(i+1,j) = 2.0*c(i,j-1) - c(i-1,j);
            end
        end
    end
else
    if isequal(order, 0)
        c = 1;
        return
    end
    if isequal(order, 1)
        c = [0, 1];
        return
    end
    c0 = zeros(1, order+1);
    c1 = chebyshev_coefficients(order-1, allorders);
    c2 = chebyshev_coefficients(order-2, allorders);
    l2 = length(c2);
    c(1) = -1.0*c2(1);
    c(order+1) = 2.0*c1(order);
    for i=2:order
        if i<=l2
            c(i) = 2.0*c1(i-1)-c2(i);
        else
            c(i) = 2.0*c1(i-1);
        end
    end    
end