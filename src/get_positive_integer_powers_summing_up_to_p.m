function tmp = get_positive_integer_powers_summing_up_to_p(n, p, info)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

if info
    greatest = p-n+1;
else
    greatest = p;
end

tmp = [];
if greatest<info
    return
end

if greatest==info
    tmp = ones(1,n)*info;
    return
end

if n==1
    tmp = p;
end

for q=greatest:-1:info 
    TMP = get_positive_integer_powers_summing_up_to_p(n-1, p-q, info);
    if isempty(TMP)
        return
    end
    tmp = [tmp ; [ ones(size(TMP,1), 1)*q TMP ] ];
end