function orders = build_tensor_grid_powers(n,d)

% INPUTS     
% n   [integer]  scalar, number of variables.
% d   [integer]  scalar, degree of approximation.

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

powers = repmat({transpose(0:d)}, 1, n);
orders = cartesian_product_of_sets(powers{:});