function x = evaluate_chebyshev_multivariate_monomials(values, powers)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.


orders = max(powers);

tmp = cell(1, size(values, 2));
for j=1:size(values, 2)
    tmp{j} = evaluate_chebyshev_monomials(values{j}, orders(j));
end

x = NaN(size(values{1}, 1), size(powers, 1));

for i=1:size(powers, 1)
    x(:, i) = 1;
    for j=1:size(powers, 2)
        x(:, i) = x(:, i).*tmp{j}(:, powers(i, j)+1);
    end
end
