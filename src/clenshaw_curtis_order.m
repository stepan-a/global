function m = clenshaw_curtis_order(i)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

m = NaN(size(i));
idx1 = find(i==1);
idx2 = find(i~=1);
m(idx1) = 1;
m(idx2) = 2.^(i(idx2)-1)+1;