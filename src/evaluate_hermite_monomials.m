function p = evaluate_hermite_monomials(values, maxorder)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

p = NaN(size(values, 1), maxorder+1);
p(:,1) = 1;
p(:,2) = values;
for order = 2:maxorder
    p(:,order+1) = values.*p(:,order)-order*p(:,order-1);
end