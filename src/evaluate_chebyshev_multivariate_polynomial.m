function f = evaluate_chebyshev_multivariate_polynomial(values, coefficients, powers)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% Get the number of variables and observations
n = size(values, 2);
T = size(values, 1);

% Dimension checks
if ~isequal(size(powers, 2), n)
    error('The number of columns in first and third arguments must match!')
end
if ~isequal(size(coefficients, 1), size(powers, 1))
    error('The number of rows in second and third arguments must match!')
end

% Get the approximation order (assumed to be constant across variables).
order = max(powers(:,1));

% Evaluate the Chebyshev monomials.
p = NaN(T, order+1, n);
for i=1:n
    p(:,:,i) = evaluate_chebyshev_monomials(values(:,i), order);
end

powers = powers+1;

% Evaluate the multivariate Chebyshev polynomial.
f = zeros(T, 1);
for i=1:length(coefficients)
    tmp = coefficients(i);
    for j=1:n
        tmp = tmp.*squeeze(p(:,powers(i,j),j)); 
    end
    f = f + tmp; 
end