function p = evaluate_chebyshev_polynomial_clenshaw(values, coefficients)

% Copyright (C) 2016 Stéphane Adjemian
% Université du Maine, GAINS & DynareTeam.
%    
% This is a free code: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version (see <http://www.gnu.org/licenses/>).
%
% This code is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

n = length(coefficients)-1;

b1 = 0;
b2 = 0;

for i=n:-1:1
    b0 = coefficients(i+1) + 2.0*values.*b1 - b2;
    b2 = b1;
    b1 = b0;
end

b0 = 2.0*coefficients(1) + 2.0*values.*b1 - b2;
p = .5*(b0-b2);